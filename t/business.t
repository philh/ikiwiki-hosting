#!/usr/bin/perl
use strict;
use warnings;
no warnings 'redefine';
use IkiWiki::Business;
use Test::More 'no_plan';

# dollar formatting
is(IkiWiki::Business::show_dollars(0), "0.00");
is(IkiWiki::Business::show_dollars(100), "1.00");
is(IkiWiki::Business::show_dollars(999), "9.99");
is(IkiWiki::Business::show_dollars(-1999), "-19.99");
is(IkiWiki::Business::show_dollars(5764), "57.64");
foreach my $n (0..9) {
	is(IkiWiki::Business::show_dollars($n), "0.0$n");
}
foreach my $n (10..99) {
	is(IkiWiki::Business::show_dollars($n), "0.$n");
}
