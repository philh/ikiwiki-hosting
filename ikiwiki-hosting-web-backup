#!/bin/bash
# Backs up all sites.

failed=""

. /etc/ikiwiki-hosting/ikiwiki-hosting.conf

LOCKFILE=/var/run/ikiwiki-hosting-web-backup-lockfile

# Use lockfile to avoid multiple jobs running.
# (bash needed because exec 200>file is a bashism)
exec 200>$LOCKFILE
if ! flock --nonblock 200; then
	echo "another ikiwiki-hosting-web-backup is already running" >&2
	exit 1
fi

trap cleanup EXIT INT

cleanup () {
	rm -f $LOCKFILE || true
	if [ -n "$backup_ssh_cache" ]; then
		kill %1 # stop any backgrounded ssh master process
	fi
}

# start master process for ssh connection caching
if [ -n "$backup_ssh_cache" ]; then
	ssh -nMN "$backup_ssh_cache" &
fi

for site in $(ikisite list); do
	if ( [ -n "$num_backups" ] && [ "$num_backups" -gt 0 ] ) || [ -n "$backup_rsync_urls" ]; then
		bdir="/var/backups/ikiwiki-hosting-web/$site"
		mkdir -p "$bdir"

		# savelog has a minimim -c of 2
		if [ -e "$bdir/backup" ] && [ -n "$num_backups" ] && [ "$num_backups" -gt 2 ]; then
			savelog -c "$num_backups" "$bdir/backup"
		fi

		if ! ikisite backup "$site" --filename="$bdir"/backup; then
			echo "ikisite backup $site failed!" >&2
			failed=1
		fi

		# rsync backups to somewhere
		if [ -n "$backup_rsync_urls" ]; then
			for url in $backup_rsync_urls; do
				if ! rsync -az $backup_rsync_options "$bdir/backup" "$url$site"; then
					failed=1
				fi
			done
		fi

		# maybe we don't want to keep backups locally..
		if [ -z "$num_backups" ] || [ "$num_backups" = 0 ]; then
			rm -f "$bdir/backup"
		fi

		# delete any obsolete version of the site in the morgue
		# (might exist if it got deleted and then recreated)
		if [ -n "$morguedir" ]; then
			rm -f "$morguedir/$site.backup"
		fi
	fi
done

if [ -n "$morguedir" ] && [ -d "$morguedir" ] && [ -n "$backup_rsync_urls" ]; then
	# backup the morgue to any configured rsync urls.
	for url in $backup_rsync_urls; do
		if ! rsync -az $backup_rsync_options "$morguedir/" "${url}morgue/"; then
			failed=1
		fi
	done
	# For each site in the morgue, zero out any old backup
	# of it that might exist on the remote. This is done to avoid
	# deleted sites being restored if the backups should be used.
	# (We can't properly delete them the way that we're using rsync.)
	for file in $(find "$morguedir" -type f); do
		site="$(basename "$file" | sed 's/\.backup$//')"
		touch "$morguedir/empty"
		if ! rsync -a $backup_rsync_options "$morguedir/empty" "$url$site"; then
			failed=1
		fi
		rm -f "$morguedir/empty"
	done
fi

if [ "$failed" ]; then
	exit 1
else
	exit 0
fi
