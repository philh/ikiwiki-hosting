# ikiwiki hosting configuration file.

# Admin contact email.
adminemail=root@localhost

# List supported domains here.
domains="example.com ikiwiki.net"

# Username prefixes used to ensure unique usernames are used for
# sites under different top level domains. Keep the prefixes short!
prefix_e=example.com
prefix_i=ikiwiki.net

# Configure a realm based on the primary supported domain, that users
# will log into via openid.
openid_realm="http://*.example.com/"

# The default version control system to use.
vcs=git

# Set to 1 to allow sites to be auto-assigned ipv4 addresses in the DNS.
allow_ipv4=0
# Set to 1 to allow sites to be auto-assigned ipv6 addresses in the DNS.
allow_ipv6=0
# (Disable all of the above to disable auto-assignment of DNS addresses.)

# by default, ikiwiki-hosting looks at the first IP on the interface
# that has the default gateway, but this may fail if you're behind NAT
# or else. those allow you to hardcode the IPs
# hardcode_ipv4=10.0.0.1
# hardcode_ipv6=fr00::0

# This is the DNS TTL to use when adding a hostname for a site.
ttl=28800

# Set to 1 to allow site admins to view analog reports.
allow_analog_reports=0

# Deleted sites can optionally be automatically backed up to a morgue directory.
#morguedir=/var/backups/ikiwiki-hosting-web/morgue

# Sites are backed up to /var/backups daily, by default. This controls
# how many days worth of backups to keep. Set to 0 to disable storing
# backups locally.
num_backups=3

# Locations that rsync should transfer site backups to.
# (In order to keep track of deleted sites, it's best to enable
# morguedir when configuring this.)
#backup_rsync_urls=

# Options to pass to rsync when it is backing up a site.
#backup_rsync_options=

# To enable ssh connection caching when making the possibly many
# rsyncs used, ssh must be started in master mode. This does so
# for the specified ssh login. Note that ssh needs to be configured
# to use a ControlPath in order for this to work.
#backup_ssh_cache=user@host

# Templates are read from this directory.
ikisite_templatedir=/etc/ikiwiki-hosting/templates

# Site configuration that site users cannot influence is read from
# per-user subdirectories of this directory.
ikisite_rootconfigdir=/etc/ikiwiki-hosting/config

# Ikiwiki auto setup files are read from this directory.
autosetupdir=/etc/ikiwiki-hosting/autosetup

# Various keys are stored in this directory.
keydir=/etc/ikiwiki-hosting/keys

# Per-site lock files are stored in this directory.
lockdir=/var/lib/ikiwiki-hosting-web/lock

# Wildcard certs for the supported domains listed above can be installed
# into subdirectories of this directory, and will be automatically used
# by ikisite when available.
wildcard_ssl_cert_dir=/etc/ikiwiki-hosting/keys/wildcard

# This can be used to set an alternative home directory in which
# to create new sites.
#useradd_basedir=/some/other/home

# Git daemon looks for vhosts in this directory.
gitdaemondir=/var/lib/ikiwiki-hosting-web/git

# Git daemon runs as this user.
gitdaemonuser=ikiwiki-anon

# Long-lived log, records site create and deletion only, for accounting.
accountinglog=/var/log/ikiwiki-hosting/accounting.log

# Hostname of site used to hold customer data.
#customersite=customers.example.com

# Hostname of site with controlpanel and makesite plugins enabled.
#controlsite=www.example.com

# List of sites that will be updated when sites are added/removed.
# These sites should each have a template named observersite.tmpl, which
# will be filled out to create pages under sites/existing/ and
# sites/removed/
#observersites="www.example.com"

# Url to ikiwiki CGI for the control site.
#controlsitecgiurl="http://www.example.com/ikiwiki.cgi"

# Where to send a user on a newly created site to welcome them to it
# and let them jumpstart with, typically, editing it.
# (Never done for branched sites, and not done if commented out.)
#welcome_redir="ikiwiki.cgi?page=index&do=edit&welcome=welcome"
# Where to send a user on a newly created blog.
#welcome_redir_blog="ikiwiki.cgi?page=posts/first_post&do=edit&welcome=welcomeblog"
