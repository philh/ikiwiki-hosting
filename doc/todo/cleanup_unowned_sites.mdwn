If a user starts to make a site, but never clicks the Finish button,
the site will be made in the background, and linger around.

Such sites can be detected because the admin is `http://none/`.

Something should handle cleaning these up. Especially since
the user may later try again with the same name.

[[done]] -- ikisite-delete-unfinished-site written, can be used in cron job
