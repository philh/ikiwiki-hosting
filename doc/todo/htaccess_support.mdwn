.htaccess files are currently ignored. Would be nice to allow
users to include them in their source, and use them.

On the ikiwiki side, that is just a matter of configuring `includes`
to make it not skip .htaccess files.

On the apache side, config has to include 'AllowOverride All'. Also
'Options SymLinksIfOwnerMatch' is needed to use the RewriteEngine from
htaccess files.

Apache's [htaccess documentation](http://httpd.apache.org/docs/1.3/howto/htaccess.html)
explains the overhead that searching for and using htaccess files entails.

We'd need to look at the security of it and make sure .htaccess files
cannot do anything bad.

A final reason not to do it, btw, is that it locks us into using apache.

[[done]] -- see [[design/apacheinclude]].
