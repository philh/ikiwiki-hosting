[[done]].. needs some testing though

<http://ikiwiki.info/tips/untrusted_git_push/>

The main sticking point on this is that git-daemon runs as a single user,
who cannot write to all the sites. Seems some sort of suid wrapper is
called for.. Or is it?

How about this:

* Add a "ikiwiki-anon" user, with locked password etc. Run git-daemon
  as that user. (done)
* In branchable plugin, add a new "anonpush" config setting.
  Should default to false, and be forced to false when branchable=0.
  (done)
* Set `untrusted_committers` to ikiwiki-anon on all sites by default.
  (done)
* Set `git_wrappermode: 6755` on all sites. (ikiwiki-anon will run the
  wrapper when a commit is pushed; need to run it as the site user) (done)
* Make ~/source.git and all its contents be writable be ikiwiki-anon.
  Somehow. Note that normal unix permissions don't suffice. (done?)
* When anonpush is enabled, enable `git_test_receive_wrapper` -- 
  and when it's disabled, disable it, and delete the hook. (That
  hook slows down all commits some.. a C wrapper that
  just checks the uid of the committer would allow speeding it back up.)
  (done)
* When anonpush is enabled, **after** wrapper setup. 
  run: `git config daemon.receivepack = true`
  (and undo when disabled). (done)
* Set core.sharedRepository to true, in all bare repos. Needed to ensure
  dirs in bare repo are always made writable by group.
  (Already done, and spot checks indicate it's working; also tested
  that it works when the sourcedir is temporarily 700 and a commit comes
  in.)
* Update do=branchable page to say when anonpush is enabled. (done)

## permissions mess

All the above is reasonable except the bit about permissions. Problem is
that both ikiwiki-anon and the site user need to be able to write to the
git repo. But it's not enough to make it 02775 siteuser:ikiwiki-anon, because
when ikiwiki-anon makes new directories like objects/xx/, it will own
them, and as the site user is not in the ikiwiki-anon group, it will not
be able to write to those subdirs, leading to later failure.
Similarly, when the site user makes commits, it will make directories owned
by it, that ikiwiki-anon cannot write to.

The site user cannot be in the ikiwiki-anon group, because then they could
access and write to other sites' repositories.

Pre-creating all possible object subdirectories and making them 
02755 siteuser:ikiwiki-anon probalby won't work well; git-gc will
delete empty object subdirectories, and it would be easy to miss some
directory that git creates.

Use ACLs? Would require filesystem support (ie, that ext234 filesystems be
mounted with 'acl' option, which does not appear to be the default). 

The magic settings seem to be:

	chmod g+s source.git # already done by git init --bare --shared
	sudo setfacl -R -m d:g:ikiwiki-anon:rwX,d:g:$siteuser:rwX,g:ikiwiki-anon:rwX,g:$siteuser:rwX source.git

This allows ikiwiki-anon to read/write to all files/dirs in the repo,
and sets all directories to propigate the ACL to new files/dirs as they
are created. When ikiwiki-anon creates a file or directory, it will
be owned by ikiwiki-anon:siteuser, and siteuser can always write to it.
