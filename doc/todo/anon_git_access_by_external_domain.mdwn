Currently, git://externl.example.com/ doesn't work. For this to work,
ikisite needs to manage links in /var/lib/ikiwiki-hosting-web/git/ for
each configured external domain. --[[Joey]] 

> [[done]] .. note that the Branchable tab still shows the internal
> domain in git urls. It's generally ok to use, unless you have some existing
> domain that is moving to Branchable, etc. --[[Joey]] 
