A site's apache configuration is automatically generated in
`/etc/apache2/sites-available/ikisite-*`.

It can be useful to tweak apache settings for a single site. To allow this,
use `/etc/ikiwiki-hosting/config/$username/apache.conf.tmpl`

This is an apache2 config fragment, that, if present, is inserted into the
site's generated apache config file by `ikisite enable`. Remember to run
`ikisite enable` if modifying it.

It is inserted inside the `<VirtualHost>` tag in the apache config file,
at the end. So it can override anything that comes before, add additional
settings, etc. It is actually a HTML::Template template, and can contain
the same variables as `apache-site.tmpl`. (HOSTNAME, HOME, etc.)

Example:

	<Directory <TMPL_VAR HOME>/public_html>
		Options +Foo
		AuthName "<TMPL_VAR HOSTNAME>"
		AuthType Basic
		AuthUserFile <TMPL_VAR HOME>/apache/htpasswd
		Require valid-user
	</Directory>
	<Directory <TMPL_VAR HOME>/public_html/.well-known>
	        AuthType None
		Require all granted
	</Directory>

Note the use of "+" to add to the default Options, rather than overriding it.

Note that the advantage of using this, over just allowing a htaccess file,
is that the site's owner could modify a htaccess file, but can't modify
this file. This avoids needing to worry about security issues with htaccess
files. (Also, apache's documentation warns about performance implications
of enabling htaccess files.) Of course, if a htaccess file is really wanted
for a given site, this file can be used to enable it.

---

To enable SSL, just install the ssl certificate to
`/etc/ikiwiki-hosting/config/$username/ssl.crt`
and the key to 
`/etc/ikiwiki-hosting/config/$username/ssl.key`.
(Be sure to only let root read this file!)
When both files exist, and it is able to verify that
the key file is valid and not password protected, `ikisite enable`
will generate an apache config that enables using ssl.

A `/etc/ikiwiki-hosting/config/$username/ssl.chain` can also be provided,
if the CA need to provide additional certificate chain material.

Note that it's up to you to configure apache to use SNI if using SSL
for multiple virtual hosts on a single IP. See
<http://wiki.apache.org/httpd/NameBasedSSLVHostsWithSNI>

If you want some Apache configuration to be applied to the SSL version
of the site but not the normal version (enabling HTTP authentication
for [[!iki plugins/httpauth desc="the httpauth plugin"]], perhaps),
put it in `/etc/ikiwiki-hosting/config/$username/apache-ssl.conf.tmpl`.

If you want to redirect the plain text (HTTP) site to the SSL version
(HTTPS), you want to check the `redirect from http to https?` option
in the config page, or `redirect_to_https` option in the `.setup` file.

---

Special configuration for the `source.$hostname.$domain` (gitweb)
virtual host can be provided too, in
`/etc/ikiwiki-hosting/config/$username/apache-source.conf.tmpl`.
