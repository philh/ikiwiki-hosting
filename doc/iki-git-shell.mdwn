# NAME

iki-git-shell - ikiwiki-hosting helper

# SYNOPSIS

iki-git-shell -c I<command> I<argument>

# DESCRIPTION

This is a wrapper for git-shell, and the parameters are passed
on to that program. Used in a ssh authorized_keys file for
an ikiwiki-hosting site user, it adds several safety features
to incoming commits. These include the ability to park a
repo, preventing changes, and limiting access to only a site's source.git
repository and no other repositories.

In addition to running git-shell commands, a very few other commands can be
run by the remote user. "logview" will tail their site's apache access.log,
and "logdump" will dump out out.

# SEE ALSO

* [[ikisite]](1)
* [[git-shell]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
