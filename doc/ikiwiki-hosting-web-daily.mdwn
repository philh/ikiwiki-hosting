# NAME

ikiwiki-hosting-web-daily - compact and maintain all sites

# SYNOPSIS

ikiwiki-hosting-web-daily

# DESCRIPTION

This script is run daily by cron, as a low-priority job.
It does various cleanup and bookkeeping tasks, including 
rotating sites logs, and updating their calendars.

# SEE ALSO

* [[ikisite]](1)

# AUTHOR

Joey Hess <joey@ikiwiki.info>

Warning: this page is automatically made into a man page via [mdwn2man](http://git.ikiwiki.info/?p=ikiwiki;a=blob;f=mdwn2man;hb=HEAD).  Edit with care
