I have successfully created a few wikis with this in the past (on 15-12-2011 to be exact), but now this seems impossible. All I get is this error:

[[!format txt """
root@marcos:~# env LANG=C ikisite  --verbose create rococo.anarcat.ath.cx --admin https://id.koumbit.net/anarcat
useradd a-rococo --create-home --skel /dev/null
git config --global user.name admin
fatal: failed to stat '.': Permission denied
error: git failed
"""]]

> Ah - it turns out that you can't run this command from a private directory (like `/root`) as ikiwiki calls `git config --global user.name admin` without changing directory first. `git`, in turn, does a `stat(".")` for some obscure reason, and fails miserably:
> 
>     [pid   597] getcwd("/root", 4096)       = 6
>     [pid   597] stat(".", 0x7fffdc602e60)   = -1 EACCES (Permission denied)
>     [pid   597] write(2, "fatal: failed to stat '.': Permi"..., 45fatal: failed to stat '.': Permission denied
>     ) = 45
>
> A simple workaround: `cd /tmp` before running the command.
> 
> So this should be fairly simple to fix: just change the directory first. It's is probably safe to `chdir($HOME)` anyways... --[[anarcat]]
>
> > I did exactly that so [[patch]] in the `dev/usercreate_fails` branch of git repo (`git://src.anarc.at/ikiwiki-hosting`). --[[anarcat]]

> > > This looks good, but incomplete: calling `ikiwiki -setup` also fails.
> > > Try my `ready/home` branch, which includes anarcat's, instead?
> > > <http://git.pseudorandom.co.uk/smcv/ikiwiki-hosting.git/shortlog/refs/heads/ready/home>
> > > --[[smcv]]

[[merged|done]] --[[Joey]]
