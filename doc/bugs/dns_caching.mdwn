If you go to $foo.branchable.com, for example to check if it exists, and
then create it, the NXDOMAIN is cached. Could be a problem later. Is there
a DNS TTL adjustment that can amelorate this? Should we have a wildcard
entry to avoid NXDOMAIN? (we could then redirect from the wildcarded host to
the actual server to deal with cached dns)

[[fixed|done]]; setting the SOA minimum to 0 fixes this by avoiding
NXDOMAIN caching, at the expense of some server load. 

(Still seeing NXDOMAIN be cached, but for just a few minutes at least
in testing. --[[Joey]])
