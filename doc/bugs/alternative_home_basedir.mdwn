I don't enable suid bits on /home on at least one system where I'd like to run ikiwiki-hosting, so for those things that need suid I create their homes on another mount (/home/suid/).  That being the case, and since there are bits of the created ikiwiki sites that need suid, I've knocked up this patch:

<pre>
From 44c545ac28f641f462bfcaac328b62b50b1e4d09 Mon Sep 17 00:00:00 2001
From: Philip Hands <phil@hands.com>
Date: Thu, 24 Nov 2011 20:21:16 +0000
Subject: [PATCH] allow sites to be created in an alternative home basedir

---
 ikisite              |    3 +++
 ikiwiki-hosting.conf |    3 +++
 2 files changed, 6 insertions(+), 0 deletions(-)

diff --git a/ikisite b/ikisite
index 7678ea2..adc5df3 100755
--- a/ikisite
+++ b/ikisite
@@ -1291,9 +1291,12 @@ sub usercreate {
 	assert_wrapper_denied();
 
 	my $user=username($hostname);
+	my @useradd_opts = ( "--base-dir", $config{useradd_basedir} )
+		if ($config{useradd_basedir}) ;
 	shell("useradd", $user, 
 		"--create-home",
 		"--skel", "/dev/null", # disable /etc/skel files
+		@useradd_opts,
 	);
 
 	mkdir(rootconfig($hostname));
diff --git a/ikiwiki-hosting.conf b/ikiwiki-hosting.conf
index 0e54189..82952eb 100644
--- a/ikiwiki-hosting.conf
+++ b/ikiwiki-hosting.conf
@@ -65,6 +65,9 @@ keydir=/etc/ikiwiki-hosting/keys
 # Per-site lock files are stored in this directory.
 lockdir=/var/lib/ikiwiki-hosting-web/lock
 
+# This can be used to set an alternative home directory in which to create new sites
+#adduser_basedir=/some/other/home
+
 # Git daemon looks for vhosts in this directory.
 gitdaemondir=/var/lib/ikiwiki-hosting-web/git
 
-- 
1.7.2.5
</pre>

Which does the job, but I'd imagine could be done more neatly, or with a better variable name.

An alternative way of fixing the specific problem might be to be able to hive off the few suid needing files into their own path, so that the /home stuff didn't need any suid bits set anyway, but even then some other people may find it useful to segregate the homes of ikiwiki users from real users on their machines -[[fil]]

> I've applied this patch. I don't see any benefit to splitting out the 
> suid bits from the rest of the home directory, and there's a very nice
> benefit in being able to move the site home directories to someplace
> other than /home. So, [[done]] --[[Joey]] 
