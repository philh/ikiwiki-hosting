I'm trying out running a copy of ikiwiki-hosting myself, in a subdomain of
`pseudorandom.co.uk` (non-commercially - I'm not trying to compete with
Branchable!). However, ikisite refuses to create subdomains, because it
assumes that entries in `domains` contain exactly one dot (so `example.com`
would be fine, but `example.co.uk` wouldn't work). This patch lets it
create domains.

[[Patch]] (which can also be cherry-picked from
`git://git.pseudorandom.co.uk/git/smcv/ikiwiki-hosting.git`):

> This is not entirely safe to apply. The problem is that
> `source.$foo.branchable.com` is a reserved domain name. If a user can 
> create that site for someone else's $foo, they can at a mimimum make
> apache unhappy with conflicting vhosts, and at the worse, engage in evil.
> 
> To do this securely, it needs to look at the `prefix_?` keys in
> `%config` and only allow one-level subdomains of domains configured
> there. --[[Joey]]

>> The next thing ikisite does is to split into hostname and domain parts,
>> using `prefix_?` as you advised: the check I'm patching here seems to
>> just be a quick way to refuse completely invalid hostnames? The user
>> can only create `source.foo.branchable.com` if a `prefix` points to
>> `foo.branchable.com` (i.e. admin error). I've tried out `source.foo`
>> for an existing and nonexistent `foo`, and I just get the error
>> `unknown prefix for foo.wiki.pseudorandom.co.uk`. --[[smcv]]

>>> Ah, good, so it does. Cherry-picked this (and most of your general
>>> changesets). [[done]] --[[Joey]]

    From 75db93d2dca5c95f0244c073c764915ee92154e3 Mon Sep 17 00:00:00 2001
    From: Simon McVittie <smcv@debian.org>
    Date: Fri, 24 Sep 2010 21:39:56 +0100
    Subject: [PATCH] ikisite: allow base hostnames with more than one dot (like example.co.uk)
    
    ---
     ikisite |    2 +-
     1 files changed, 1 insertions(+), 1 deletions(-)
    
    diff --git a/ikisite b/ikisite
    index b1e9cb5..dcac43f 100755
    --- a/ikisite
    +++ b/ikisite
    @@ -747,7 +747,7 @@ sub username {
     	my $hostname=lc(shift);
     	
     	# paranoia: make sure the hostname looks like a FQDN
    -	if ($hostname !~ /^([a-z0-9][-a-z0-9]*)\.\w+\.\w+$/) {
    +	if ($hostname !~ /^([a-z0-9][-a-z0-9]*)\.\w+(\.\w+)+$/) {
     		error "illegal hostname \"$hostname\"";
     	}
     	if (length $hostname > 252) { # why not 253? Trailing dot..
    -- 
    1.7.1
