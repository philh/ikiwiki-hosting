Report from blind user, was unable to find delete button for sites on the
control panel.

Probably because the javascript only makes buttons to manage a site visible when
the mouse is hovering over that site in the list. When I turn on caret
browsing and use arrow keys to move over sites, I experience the same
problem.

The point of the CSS is to reduce visual clutter with the several buttons
per site.. That is less important
than accessability, but still seems there should be a way to get both.

Note that the control panel is designed to work when javascript is
disabled. The buttons default to visible until javascript hides them.

fix: Instead of totally hiding these buttons, display them with a
low opacity. So screen readers can still speak them. [[done]]
--[[Joey]]
