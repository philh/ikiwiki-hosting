Source: ikiwiki-hosting
Section: admin
Priority: optional
Build-Depends:
 debhelper (>= 11~),
 ikiwiki,
Maintainer: Simon McVittie <smcv@debian.org>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/debian/ikiwiki-hosting.git
Vcs-Browser: https://salsa.debian.org/debian/ikiwiki-hosting
Homepage: https://ikiwiki-hosting.branchable.com/

Package: ikiwiki-hosting-common
Architecture: all
Depends:
 ${misc:Depends},
 ${perl:Depends},
Description: ikiwiki hosting: common files
 A hosting interface for ikiwiki. Facilitates management of many separate
 ikiwiki sites, with capabilities including web-based signup to create new
 sites, easy support for branching sites, deleting sites, and transferring
 sites between servers. Ikiwiki-hosting was developed for Branchable.com.
 .
 This package contains common files for all ikiwiki hosting servers,
 and documentation.

Package: ikiwiki-hosting-dns
Architecture: all
Depends:
 bind9,
 ikiwiki-hosting-common,
 ${misc:Depends},
 ${perl:Depends},
Description: ikiwiki hosting: dns server
 A hosting interface for ikiwiki. Facilitates management of many separate
 ikiwiki sites, with capabilities including web-based signup to create new
 sites, easy support for branching sites, deleting sites, and transferring
 sites between servers. Ikiwiki-hosting was developed for Branchable.com.
 .
 This package should be installed on the master DNS server, only if you
 will be allowing ikiwiki-hosting to automatically manage DNS for
 sites. It contains the ikidns program.

Package: ikiwiki-hosting-web
Architecture: any
Depends:
 acl,
 adduser,
 apache2,
 apache2-suexec-pristine | apache2-suexec,
 dnsutils,
 gcc | c-compiler,
 git (>= 1:1.7.0),
 gitweb (>= 1:1.7.0),
 ikiwiki (>= 3.20100723),
 ikiwiki-hosting-common,
 libauthen-passphrase-perl,
 libc6-dev | libc-dev,
 libcgi-formbuilder-perl (>= 3.05),
 libcgi-pm-perl,
 libcgi-session-perl (>= 4.14-1),
 libcoy-perl,
 libcrypt-ssleay-perl,
 libdata-compare-perl,
 libdatetime-perl,
 libfile-mimeinfo-perl,
 libgravatar-url-perl,
 libhighlight-perl,
 libhtml-tree-perl,
 libimage-magick-perl | perlmagick,
 liblocale-gettext-perl (>= 1.05-1),
 liblwpx-paranoidagent-perl,
 libmail-sendmail-perl,
 libmailtools-perl,
 libnet-inet6glue-perl,
 libnet-openid-consumer-perl (>= 1.03),
 librpc-xml-perl,
 libtext-csv-perl,
 libtext-markdown-perl (>= 1.0.26-1~bpo50+1),
 libtext-multimarkdown-perl,
 libtext-textile-perl,
 libtext-typography-perl,
 libtext-wikicreole-perl,
 libtext-wikiformat-perl,
 libtimedate-perl,
 libxml-feed-perl,
 libxml-simple-perl,
 libyaml-perl,
 libyaml-syck-perl,
 lsb-base,
 moreutils (>= 0.43),
 openssl,
 polygen,
 polygen-data,
 python3,
 python3-docutils,
 uuid,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Recommends:
 analog,
 certbot,
Conflicts:
 parallel,
Description: ikiwiki hosting: web server
 A hosting interface for ikiwiki. Facilitates management of many separate
 ikiwiki sites, with capabilities including web-based signup to create new
 sites, easy support for branching sites, deleting sites, and transferring
 sites between servers. Ikiwiki-hosting was developed for Branchable.com.
 .
 This package contains the ikisite program, and related things to install
 on each web server.
